<?php
class TraineeDetails
{                                                                                                                                                 public $id;                   
	public $userName; //שם משתמש - מפתח ראשי  
	public $password; //סיסמא
	public $email;  //אימייל
	public $status;  //מתאמן מתחיל . מתאמן במידה מועטה . מתאמן פעיל . מתאמן פעיל מאוד *** משתנה מסוג מספר בין 1 ל 4***
	public $first_name;//שם פרטי
    public $last_name;//שם משפחה
    public $target;//מטרה של מתאמן -- השמנה \ הרזיה \ חיטוב \אורח חיים בריא 
    public $age;//גיל 
    public $gender;//מין
    public $weight;//משקל **חובה
    public $height;//גובה **חובה
    public $body_fat;//אחוזי שומן
    public $health_condition;//מצב בריאותי ** מילוי שאלון אוטומטי שמדרג את המתאמן לפי התשובות שהוא מכניס 
    // בחירה של אופציה לביטול איזור בגוף -- למשל למתאמן מסוים אסור לאמץ את הרגליים אז לתת לו לבחור שאין התייחסות לאזור הרגליים
    public $intensity;//האם המתאמן רוצה אימון בעצימות גבוהה
}

?>