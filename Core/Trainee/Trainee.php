<?php
//Trainee class 
//require "../Core/Exercise/Exercise.php"; 
//require "../Core/Exercise/ICat.php";
//require "../Core/User/User.php";
//require "../Core/Trainee/Trainee.php";
//require "../Core/Program/Program.php";
//require "../Core/Trainee/TraineeDetails.php";
//require "../Core/Session/Session.php";

class Trainee
{                                                                                                                                                                            
    public $traineeDetails;
    public $strength;

    
	 public function bmi(){
         return (integer)( $this->traineeDetails->weight / ($this->traineeDetails->height * $this->traineeDetails->height) );
     }
	
    public function getStrength(){//חישוב כח של מתאמן
        $this->strength = 0;
        $bmiRange = array(4=>array(7,8,9,10,11,12,13),3=>array(14,15,16,17,18),2=>array(19,20,21,22),1=>array(23,24,25,26,27,28,29,30));
        //מערך של טווח המספרים בסקלת הבי אמ אי
        $bodyFatRange = array(3=>array(20,21,22,23,24,25),2=>array(26,27,28,29,30),1.5=>array(31,32,33,34,35),1=>array(36,37,38,39,40));
        //מערך של טווח המספרים בסקלת אחוזי שומן
        if($this->traineeDetails->gender == 'm')//אם המתאמן זכר אז לקדם את הכח ב1
            $this->strength++;
        $this->strength += $this->traineeDetails->status / 2;//מוסיף כח לפי סטטוס מתאמן כאשר פעיל מואד 4 יקבל 2 נק בכח ואילו מתאמן ברמה 3 יקבל 1.5 לכח וכך הלאה
        $bmi = $this->bmi();//חישוב הבי אמ איי
//        echo $bmi;
        foreach($bmiRange as $element){// בדיקה האם ה בי אמ איי בטווח
            $this->strength += array_search($bmi,$element);
        }
        foreach($bodyFatRange as $element2){// בדיקה האם אחוזי שומן בטווח והוספה לפי הצורך
        if(isset($this->traineeDetails->body_fat))
            $this->strength += array_search($this->traineeDetails->body_fat,$element2);
        }
        
    }
    
    
	public function buildProg(){
        $prog1 = new Program();
        $prog1->progName = 'RefitProg';
        $prog1->userName = $this->traineeDetails->userName;
        $prog1->startDate = date("Y/m/d");
        $prog1->endDate = strtotime($prog1->startDate);
        $prog1->endDate = strtotime("+6 weeks" , $prog1->endDate);
        $prog1->endDate = date('Y/m/d', $prog1->endDate);
        return $prog1;
    }
    
    public function buildObjectExInProgramArray(&$selectedExercises,&$objectExInProgramArray){
        foreach($selectedExercises as $element)
        {
            foreach($element->exercises as $element2){
                $objectExInProgram = new objectExInProg();
                if($element->category == 'legs'){
                    $objectExInProgram->reps = 12;
                    $objectExInProgram->sets = 4;
                    $objectExInProgram->rest = 30;//3min
                    $objectExInProgram->weight = $this->strength*16;
                    $objectExInProgram->categoryGroup = 'A';
                }
                if($element->category == 'chest'){
                    $objectExInProgram->reps = 12;
                    $objectExInProgram->sets = 4;
                    $objectExInProgram->rest = 30;
                    $objectExInProgram->weight = $this->strength*10;
                    $objectExInProgram->categoryGroup = 'A';
                }
                if($element->category == 'shoulders'){
                    $objectExInProgram->reps = 12;
                    $objectExInProgram->sets = 4;
                    $objectExInProgram->rest = 30;//3min
                    $objectExInProgram->weight = $this->strength*2.5;
                    $objectExInProgram->categoryGroup = 'B';
                }
                if($element->category == 'back'){
                    $objectExInProgram->reps = 12;
                    $objectExInProgram->sets = 4;
                    $objectExInProgram->rest = 30;//3min
                    $objectExInProgram->weight = $this->strength*3;
                    $objectExInProgram->categoryGroup = 'B';
                }
                if($element->category == 'bicep'){
                    $objectExInProgram->reps = 12;
                    $objectExInProgram->sets = 4;
                    $objectExInProgram->rest = 30;//3min
                    $objectExInProgram->weight = $this->strength*2;
                    $objectExInProgram->categoryGroup = 'A';
                }
                if($element->category == 'tricep'){
                    $objectExInProgram->reps = 12;
                    $objectExInProgram->sets = 4;
                    $objectExInProgram->rest = 30;//3min
                    $objectExInProgram->weight = $this->strength*2;
                    $objectExInProgram->categoryGroup = 'B';
                }
                if($element->category == 'abdominals'){
                    $objectExInProgram->reps = 12;
                    $objectExInProgram->sets = 4;
                    $objectExInProgram->rest = 30;//3min
                    $objectExInProgram->weight = $this->strength*1.5;
                    $objectExInProgram->categoryGroup = 'D';
                }
                if($element->category == 'special'){
                    $objectExInProgram->reps = 12;
                    $objectExInProgram->sets = 4;
                    $objectExInProgram->rest = 30;//3min
                    $objectExInProgram->weight = $this->strength*2;
                    $objectExInProgram->categoryGroup = 'C';
                }
                
                $objectExInProgram->exerciseName = $element2->exerciseName;
                array_push($objectExInProgramArray,$objectExInProgram);
            }
            
            
            
        }
    }
    
    public function autoProg(){
        $db1 = new dbClass();//DB Object
        $exersicesNames = array('legs','chest','shoulders','back','bicep','tricep','abdominals','special');
        $selectedExercises = array();
         $selectedExercises = $this->buildExStructure($selectedExercises);
        
        $inputArray = $db1->getAllExersices();//Connect to Database and Bring all Exercises
        
        
        $this->getStrength();//Get Strength of this Trainee

        foreach($inputArray as $value){//loop that goes all ICat
            for($i = 0 ; $i < count($value->exercises)  ; $i++){//loop that goes All Exercise inside Each Category
                if($this->strength >= $value->exercises[$i]->complexMin &&
                  $this->strength <= $value->exercises[$i]->complexMax)//Check each Exercise if The Trainee Strength is in The Complex Renge.
                    array_push($selectedExercises[array_search($value->exercises[$i]->catagory,$exersicesNames)]->exercises
                               ,$value->exercises[$i]);//if is in the range --> add to Temp program (selectedExercises)
            }
        }
        
        $program = $this->buildProg();//Build Defualt Program 
        $selectedExercises = $this->randProg($selectedExercises);//Randomaly Set Exercises in different places in the array
        foreach($selectedExercises as $element){//Choose only 2 Exercises in Each Category
            for($i = count($element->exercises)-1 ; $i >= 2 ; $i-- ){//**get number of exercises to decide how much in each category
                array_pop($element->exercises);
            }
        }

        $program->calcTraineeGrade($this->strength);//Calc TraineeGrade At the first Time -- Every Complete Program We Update The Grade To Target
        $program->calcProgGrade($this->strength);//Calc ProgramGrade To Be a Target
        
        
        $objectExInProgramArray = array();
        $this->buildObjectExInProgramArray($selectedExercises,$objectExInProgramArray);
        $program->excercises = $objectExInProgramArray; // Get Selected Exercises 
        $db1->insertProgramByTrainee($program); // Insert to DataBase
    }
    
    
    public function randProg($inputArray){
        $tempExercises = array();
        $tempExercises = $this->buildExStructure($tempExercises);
        
        for($i = 0 ; $i < count($inputArray) ; $i++){
            for ($j = 0; $j < count($inputArray[$i]->exercises) ; $j++)
            {
                $index = rand()%(count($inputArray[$i]->exercises)-$j);
                $tempExercises[$i]->exercises[$j] = $inputArray[$i]->exercises[$index];
                $this->swap($inputArray[$i]->exercises,$index,count($inputArray[$i]->exercises)-1-$j);
            }
        }
        return $tempExercises;
    }
    
    public function swap($inputArray,$index1,$index2){
        $temp = $index2;
        $inputArray[$index2] = $inputArray[$index1];
        $inputArray[$index1] = $inputArray[$temp];
    }
    
    public function buildExStructure($arr){
        $exersicesNames = array('legs','chest','shoulders','back','bicep','tricep','abdominals','special');
        
        for( $i = 0 ; $i < count($exersicesNames) ; $i++ ){// build the Structure Of Exercises
            $arrayOfCategories = new ICat();
            $arrayOfCategories->category = $exersicesNames[$i];
            array_push($arr,$arrayOfCategories);
        }
        return $arr;
    }
    
}

?>