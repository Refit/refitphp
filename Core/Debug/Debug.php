<?php
/**
 * Created by PhpStorm.
 * User: Sigal ben
 * Date: 06/06/2018
 * Time: 17:44
 */

class Debug
{
    public $id;
    public $errorType;
    public $userName;
    public $date;
    public $info;
    public $status;
    public $takeCare;
    public $component;
    public $moduleName;
}

?>