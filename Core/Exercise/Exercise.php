<?php


 class Exercise 
    {   
        public $exerciseName;//שם תרגיל **מפתח ראשי
        public $catagory;//קטגוריה מבין אזורים בגוף
        public $exerciseType;//סוג התרגיל מתוך הקטגוריה הנבחרת
        public $instructions;//הוראות ביצוע התרגיל
        public $video;//קישור לוידאו אשר מדגים את התרגיל
        public $picture;//קישור לתמונה
        public $recommanded;//האם התרגיל מומלץ על ידי המערכת
        public $mistakes;//טעויות נפוצות איך לא לעשות את התרגיל
        public $activityGroup = array();//קבוצה אקטיבית של שרירים העובדים בתרגיל
        public $complexMin;//מינימום סיבוך של תרגיל
        public $complexMax;//מקסימום סיבוך של תרגיל
        public $health;//מספר שלם אשר מייצג את רמת הבריאות שמצריך התרגיל הזה - הטווח נע בין 0 ל 4 
        public $motivation;//מטרה
        public $isFast;//האם התרגיל מקדם את הגוף למטרה בצורה מהירה יותר ביחס לשאר התרגילים

    }

?>