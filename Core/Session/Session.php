<?php

//רשומה אשר מכילה פרטים על סשן של מתאמן מסוים
class Session
{       
    public $prog_id;
    public $exerciseName;
    public $reps;
    public $sets;
    public $rest;
    public $weight;
    public $sessionDate;
}

?>