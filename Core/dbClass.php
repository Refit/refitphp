<?php

require "../Core/Exercise/Exercise.php"; 
require "../Core/Exercise/ICat.php";
require "../Core/User/User.php";
require "../Core/Trainee/Trainee.php";
require "../Core/Program/Program.php";
require "../Core/Trainee/TraineeDetails.php";
require "../Core/Session/Session.php";
require "../Core/Debug/Debug.php";
require "../Core/program_exercise/objectExInProg.php";

class dbClass
{
    //Variables to connect to the database
	private $host;
	private $db;
	private $charset;
	private $user;
	private $pass;
	private $opt = array(
	        PDO::ATTR_ERRMODE  =>PDO::ERRMODE_EXCEPTION,
	        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
	private $connection;

	//constructor
	public function __construct($host = "localhost" , $db = "refit_db" ,
	                             $charset = "utf8" , $user = "root" , $pass = "" )
								{
									$this->host = $host;
									$this->db = $db;
									$this->charset = $charset;
									$this->user = $user;
									$this->pass = $pass;
								}
    //function connect to DB
	private function connect()
	{
		$dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
		$this->connection = new PDO($dsn,$this->user,$this->pass,$this->opt);

	}

    //function disconnect to DB
	public function disconnect()//function disconnect from DB
	{
		$this->connection = null;
	}

    //Build a structure of exercises and get from the database all the exercises
    public function getAllExersices() {
        $this->connect();//connect to the Database
        $exersicesNames = array('legs','chest','shoulders','back','bicep','tricep','abdominals','special');//************
        $arrayOfExersices = array();
        for( $i = 0 ; $i < count($exersicesNames) ; $i++ ){//loop that build the strucre that i want in angular
            $arrayOfCategories = new ICat();
            $arrayOfCategories->category = $exersicesNames[$i];
            array_push($arrayOfExersices,$arrayOfCategories);
        }

		$result = $this->connection->query("SELECT * FROM exercises");//SQL query - get all exercises

		while($row=$result->fetchObject('Exercise')){//loop that each row from the SQL table - convert to class and push to array
            if(isset($row->activityGroup))// if the field is array
                $row->activityGroup = explode(',',$row->activityGroup);
            array_push($arrayOfExersices[array_search($row->catagory,$exersicesNames)]->exercises,$row);
		}
		$this->disconnect();   //disconnect to the DataBase
        return $arrayOfExersices;//return allExersices
    }
    //Bring all users from the database
    public function getAllUsers()
    {
        $this->connect();
		$statement=$this->connection->query("SELECT * FROM users");
        $users = array();
        $i = 0;
        while($User=$statement->fetchObject('User')){
            $users[$i] = $User;
            $i++;
        }
        $this->disconnect();
		return $users;
    }

    //Insert a user / trainee into the database only if it does not exist,
    // insert -- user table && table of trainees
    public function insertUser($user)
    {
        $this->connect();
        $result = $this->connection->query("SELECT id FROM users WHERE userName='$user->userName'");
        $id = $result->fetch(PDO::FETCH_ASSOC);
        if($id['id'] == null){
            $pass = password_hash($user->password,PASSWORD_BCRYPT);

            $check1 = $this->connection->query("INSERT INTO users (id, userName, password, email, isAdmin) VALUES
                                                (NULL, '$user->userName',
                                                '$pass','$user->email','$user->isAdmin')");
            $check2 = $this->connection->query("INSERT INTO trainees (id, userName,
                                              email, status, first_name, last_name, target, age,
                                               gender, weight, height, body_fat, health_condition, intensity) VALUES 
                                              (NULL, '$user->userName', '$user->email',
                                               NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");
            if($check1 != null && $check2 != null){
                $result = $user->password;
            }else{
                $result = 'Db Error';
            }
        }else{
            $result = 'Exists';
        }
        $this->disconnect();
        return $result;

    }
    //Check whether the user exists in the database
	public function checkVerify($userName,$password)
    {
        $this->connect();
		$statement=$this->connection->query("SELECT password,isAdmin FROM users WHERE userName='$userName'");
		$user=$statement->fetch(PDO::FETCH_ASSOC);
		// check user is  in the DB, and password is valid
		if( password_verify($password,$user['password']))
			$res = $user['isAdmin'];
		else // user doesn't exist or password doesn't match
			$res = 'error';
        $this->disconnect();
		return $res;

    }
    //A selection from the database trains by ID number
	public function getTraineebyId($userName)
	{
		$this->connect();
		$statement=$this->connection->query("SELECT * FROM trainees WHERE userName='$userName'");
		$result=$statement->fetchObject('TraineeDetails');
		$trainee = new Trainee();
		$trainee->traineeDetails = $result;

		$this->disconnect();
		return $trainee;

	}
    //Inserting the database into program
	public function insertProgram($program)
    {
        $this->connect();
        $res = $this->connection->query("INSERT INTO programs (prog_id, prog_name, user_name, start_date,
                                          end_date, prog_grade, trainee_grade) VALUES
                                          ('$program->prog_id','$program->progName','$program->userName',
                                          '$program->startDate','$program->endDate','$program->progGrade',
                                          '$program->traineeGrade')");
        $this->disconnect();
        if($res != null){
            return 'Success';
        }
        return 'error';
    }


    //Inserting an exercise into the database
    public function insertExercise($ex)
    {
        $this->connect();
        $res = $this->connection->query("INSERT INTO exercises(exerciseName, catagory, exerciseType,
                                       instructions, video, picture, recommanded, mistakes, activityGroup,
                                        complexMin, complexMax, health, motivation, isFast) VALUES
                                         ('$ex->exerciseName','$ex->catagory','$ex->exerciseType',
                                         '$ex->instructions','$ex->video','$ex->picture','$ex->recommanded',
                                         '$ex->mistakes','$ex->activityGroup','$ex->complexMin',
                                         '$ex->complexMax','$ex->health','$ex->motivation','$ex->isFast')");
        $this->disconnect();
        if($res != null){
            return 'Success';
        }
        return 'error';
    }
    //Deleting a exercise to the database
    public function deleteExerciseByName($exerciseName){

         $this->connect();

        $res = $this->connection->query("DELETE FROM exercises WHERE exerciseName = '$exerciseName'");

		$this->disconnect();

        if($res != null){
            return 'Success';
        }
        return 'error';
    }
    
    
    //EDIT exercise to the database
    public function editExbyName($ex){
        $this->connect();
        $this->connection->query("UPDATE exercises SET 
                                catagory = '$ex->catagory',exerciseType = '$ex->exerciseType',
                                instructions = '$ex->instructions',video='$ex->video',
                                picture = '$ex->picture', recommanded ='$ex->recommanded',
                                mistakes ='$ex->mistakes', activityGroup = '$ex->activityGroup',
                                complexMin ='$ex->complexMin', complexMax ='$ex->complexMax',
                                health ='$ex->health', motivation = '$ex->motivation', isFast ='$ex->isFast'
                                WHERE exerciseName= '$ex->exerciseName'");


        $this->disconnect();
    }
    //Insert a session into the database
    public function insertSession($sessionArray){
        $res=null;
        $this->connect();
        $func = $this->connection;
        foreach($sessionArray as $element){
                        $res = $func->query("INSERT INTO session (prog_id,exerciseName,reps,sets,rest,weight,sessionDate) VALUES
                                            ('$element->prog_id','$element->exerciseName','$element->reps',
                                            '$element->sets','$element->weight','$element->rest','$element->sessionDate')");
                        if($res == null){
                            $res = 'error';
                            break;
                        }
            }
        $this->disconnect();
        return $res == 'error' ? $res : 'Success';
    }
    //**Insert a program by trainee into the database**
    //insert first program to program table
    //insert to Array Of Exercises Belong To specific Trainee
    public function insertProgramByTrainee($program){
        $res=null;
        $this->connect();
        $func = $this->connection;

        if($this->insertProgram($program) != null){
            $id = $this->getProgramById($program->userName);
            foreach($program->excercises as $element){

                $currEx = $element->exerciseName;
                $reps = $element->reps;
                $sets = $element->sets;
                $rest = $element->rest;
                $weight = $element->weight;
                $group = $element->categoryGroup;
                $id1 = $id['prog_id'];
                $res = $func->query("INSERT INTO programs_exercises (program_id, exerciseName,
                                          reps,sets,weight,rest,categoryGroup) VALUES
                                           ('$id1','$currEx','$reps','$sets','$weight','$rest','$group')");
                if($res == null){
                    $res = 'error';
                    break;
                }
            }
        }
        $this->disconnect();
        return $res == 'error' ? $res : 'Success';
    }
    //Choose an array of exercises by a specific Program
    public function getObjExInProg($programId){
        $this->connect();
        $id = $programId['prog_id'];
        $statement = $this->connection->query("SELECT * FROM programs_exercises WHERE program_id='$id'");
        $objExInProgArray = array();
        $i = 0;
        while($objExInProg=$statement->fetchObject('objectExInProg')){
            $objExInProgArray[$i] = $objExInProg;
            $i++;
        }
        $this->disconnect();
		return $objExInProgArray;
    }
    //Choose program by specific Trainee Id
    public function getProgramById($userName)
        {
            $this->connect();
            $result = $this->connection->query("SELECT prog_id FROM programs WHERE user_name='$userName'");
            $row = $result->fetch(PDO::FETCH_ASSOC);
            $this->disconnect();
            return $row;
        }
    //Delete Program by Id
    // First Delete Childrens and After Program
    public function deleteProgramById($prog_id){
        $this->connect();
        $res1 = $this->connection->query("DELETE FROM programs_exercises WHERE program_id='$prog_id'");//child
        $res1 = $this->connection->query("DELETE FROM sessions WHERE prog_id='$prog_id'");//child
        $res2 = $this->connection->query("DELETE FROM programs WHERE prog_id='$prog_id'");

        $this->disconnect();
        if($res1 != null && $res2 != null){
            return 'Success';
        }
        return 'error';
    }
    //Update User Details - Email / IsAdmin -- By userName
    public function editUserbyName($user){
         $this->connect();
        $res = $this->connection->query("UPDATE users SET email='$user->email', isAdmin='$user->isAdmin' WHERE
                                        userName = '$user->userName'");
        $this->connection->query("UPDATE trainees SET email='$user->email' WHERE userName = '$user->userName'");
        $this->disconnect();
        if($res != null)
            return 'Success';
        return 'error';
    }


    public function deleteUserByName($userName){
        
         $this->connect();

        $result = $this->connection->query("DELETE FROM users WHERE userName = '$userName'");
		
		$this->disconnect();
        
        return 1;
    }

     public function getDebugTable(){
         $this->connect();
         $statement=$this->connection->query("SELECT * FROM admin_debug");
         $debugArray = array();
         $i = 0;
         while($Debug=$statement->fetchObject('Debug')){
             $debugArray[$i] = $Debug;
             $i++;
         }
         $this->disconnect();
         return $debugArray;
     }








}

?>