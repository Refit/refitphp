<?php

class Program
{       
    public $prog_id;
    public $progName; //שם אימון
    public $userName; 
    public $startDate;//תאריך התחלה של אימון
    public $endDate;//תאריך סיום של אימון 
    public $progGrade;//מספר אשר מייצג את הסכם של אותו אימון - כלמור זה מייצג את היעד של התוכנית לאן המתאמן צריך להגיע
    public $traineeGrade;//מספר אשר מיצג את הסכם של המתאמן הכללי על פי חישוב נתונים אישיים -- חייב להיות קטן מהסכם של התכונית כי אז לא תיהיה מטרה
    public $excercises;//מערך של תרגילים אשר קיבלנו או מחישוב על פי נתונים אישיים
    public $sessions = array();
    
    
    public function calcTraineeGrade($strength){
        $this->traineeGrade = $strength;
    }
    
    public function calcProgGrade($strength){
        $this->progGrade = $strength*1.2;
    }
    
    
    public function firstSession($userName){
        $db = new dbClass();
        $this->userName = $userName;
        $prog_id = $db->getProgramById($this->userName);
        $this->excercises = $db->getObjExInProg($prog_id);
//        var_dump($this->excercises);
       // $this->buildSession();
        //$db->insertSession($this->sessions);
            
    }
    
     public function buildSession(){
        foreach($this->excercises as $element)
        {
                    $sessionProgram = new Session();
                    $sessionProgram->prog_id = $element->program_id;
                    $sessionProgram->exerciseName = $element->exerciseName;
                    $sessionProgram->reps = 12;
                    $sessionProgram->sets = 4;
                    $sessionProgram->rest = 30;//3min
                    $sessionProgram->weight = 5;
                    $sessionProgram->sessionDate = date("Y/m/d");
            
            array_push($this->sessions,$sessionProgram);
        }
                
    }
    
}

?>