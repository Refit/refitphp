-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2018 at 03:07 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `refit_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_debug`
--

CREATE TABLE `admin_debug` (
  `id` int(11) UNSIGNED NOT NULL,
  `errorType` varchar(32) NOT NULL,
  `userName` varchar(32) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `info` text NOT NULL,
  `status` int(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exercises`
--

CREATE TABLE `exercises` (
  `exerciseName` varchar(22) NOT NULL,
  `catagory` varchar(11) NOT NULL,
  `exerciseType` varchar(11) DEFAULT NULL,
  `instructions` text,
  `video` text,
  `picture` text,
  `recommanded` tinyint(1) DEFAULT NULL,
  `mistakes` text,
  `activityGroup` text,
  `complexMin` tinyint(1) DEFAULT NULL,
  `complexMax` tinyint(1) DEFAULT NULL,
  `health` tinyint(1) DEFAULT NULL,
  `motivation` varchar(11) DEFAULT NULL,
  `isFast` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exercises`
--

INSERT INTO `exercises` (`exerciseName`, `catagory`, `exerciseType`, `instructions`, `video`, `picture`, `recommanded`, `mistakes`, `activityGroup`, `complexMin`, `complexMax`, `health`, `motivation`, `isFast`) VALUES
('Arm Banding', 'bicep', 'Weights', 'Directions for sitting: Laying the forearm on the thigh and lifting the palm while holding the weight.\r\nIt can be done with two facilitators:\r\n1. The palm of the hand is directed to the ceiling - training the hand flexors\r\n2. The palm of the hand is directed to the ceiling - training the hand wrists.', '', '', 1, '', 'Biceps', 4, 8, 0, '', 0),
('Banet Over Row', 'back', 'Weights', 'not recommended. Standing on the back is parallel to the floor, bending the shoulder and elbow\r\nThese exercises are mainly designed to strengthen the Dorsi Latissimus.', '', '', 0, '', 'Latissimus Dorsi , Biceps , Trapezius', 5, 9, 0, '', 0),
('Bankruptcy', 'Legs', 'Lower Poly', 'This exercise is primarily intended to hold the Maximus Gluteus. Position: Standing, tilting forward. The working leg begins,\r\nWhile bending the thigh and ending with a full-range raid (overflowing - up to 10-20 degrees.', '', '', 0, '', 'Gluteus maximus', 4, 8, 0, '', 0),
('Bench press machine', 'chest', 'Machine', 'A bench press in a dedicated machine\r\nHorizontal approximation. You can change the\r\nNarrow grip handles\r\nAnd straight. \"Position Mid\" - Purpose\r\nThis exercise is Triceps action.\r\nChest operation will be secondary.\r\nNotice that the elbow is high\r\nShoulder or a little below. Match\r\nThe height of the chair is slightly bent\r\nBelow the chest line. The foot\r\nServes as a concentric work.', '', '', 1, '', 'Pectoralis major , Triceps, Seratus Anterior', 0, 6, 0, '', 0),
('Bend the elbows', 'bicep', 'Weights', 'These exercises are simple exercises for the elbow flexors. Menach: Standing, standing forward, so that at the height of the movement,\r\nThe cargo arm will be inside the stability base, elbows close to the body, the bending will be carried out within full range of motion. Can be backwards\r\nIn a number of grips: broad, narrow, supination or pronation. It is important to have a comfortable grip, supination, a greater range of motion,\r\nWhen the bar is vertical to the body will be the hardest because the largest arm of the load.\r\n\r\nHighlights with hand weights:\r\nThe range of motion is greater.\r\nYou can bend the calculus at once, either\r\nHand (alternate) so that it will be possible to develop more\r\nDue to a lateral loss, a two-sided loss.', '', '', 1, 'Common Mistakes:\r\nStanding with a slight bend in your knees will not reduce the load on your back. If you want to improve stability, you have to be punctual.\r\nLifting elbows at the height of contraction - Bicep is completely constricted and therefore will not help shoulder flexion. As well as momentum, relief and timely execution\r\nThe application angle of the elbow flexors is more flat and therefore more difficult to perform.', 'Bicep , Brichialis , Anterior Deltoid , Hamstring , Erector Spinae', 0, 5, 0, '', 0),
('Bend the elbows bar', 'bicep', 'Weights', 'These exercises are simple exercises for the elbow flexors. Menach: Standing, standing forward, so that at the height of the movement,\r\nThe cargo arm will be inside the stability base, elbows close to the body, the bending will be carried out within full range of motion. Can be backwards\r\nIn a number of grips: broad, narrow, supination or pronation. It is important to have a comfortable grip, supination, a greater range of motion,\r\nWhen the bar is vertical to the body will be the hardest because the largest arm of the load.\r\n\r\nHighlights with bar:\r\nCan be performed with a straight rod or rod\r\nW, the W pole allows grip\r\nApproaching - Mid', '', '', 1, 'Common Mistakes:\r\nStanding with a slight bend in your knees will not reduce the load on your back. If you want to improve stability, you have to be punctual.\r\nLifting elbows at the height of contraction - Bicep is completely constricted and therefore will not help shoulder flexion. As well as momentum, relief and timely execution\r\nThe application angle of the elbow flexors is more flat and therefore more difficult to perform.', 'Bicep , Brichialis , Anterior Deltoid , Hamstring , Erector Spinae', 4, 8, 0, '', 0),
('Bend the Elbows Poly', 'bicep', 'Weights', 'These exercises are simple exercises for the elbow flexors. Menach: Standing, standing forward, so that at the height of the movement,\r\nThe cargo arm will be inside the stability base, elbows close to the body, the bending will be carried out within full range of motion. Can be backwards\r\nIn a number of grips: broad, narrow, supination or pronation. It is important to have a comfortable grip, supination, a greater range of motion,\r\nWhen the bar is vertical to the body will be the hardest because the largest arm of the load.\r\n\r\nHighlights with Polly\r\nBottom, Face for Polly:\r\nVariable angle of resistance and angle\r\nThe rotating force.\r\nYou should stand close to Polly\r\nDo not put unnecessary pressure on your back\r\nAnd that the traffic range is complete.', '', '', 1, 'Common Mistakes:\r\nStanding with a slight bend in your knees will not reduce the load on your back. If you want to improve stability, you have to be punctual.\r\nLifting elbows at the height of contraction - Bicep is completely constricted and therefore will not help shoulder flexion. As well as momentum, relief and timely execution\r\nThe application angle of the elbow flexors is more flat and therefore more difficult to perform.', 'Bicep , Brichialis , Anterior Deltoid , Hamstring , Erector Spinae', 3, 7, 0, '', 0),
('Cable Cross close', 'chest', 'Weights', 'This exercise is intended primarily to strengthen the Major Pectoralis Lower. Standing: standing up, Polly up, step to the bars\r\nHalf knelt forward, tilting the go, elbows back. It is important that the elbow be at a higher angle than the palm of the hand. Belly contraction.\r\nMove downward. It is important to maintain a neutral and permanent position on the spine.\r\nAt the beginning of the movement the discharge component, rotary, compressive.', '', '', 0, '', 'Gluteus Maximus , BBB elbows', 5, 9, 0, '', 0),
('Cable Flay', 'chest', 'Bench press', 'Butterfly with Cross Cable from Lower Bolts\r\nLet\'s put a bench and use handles from the situation\r\nLower, in the same way of use\r\nIn free weights. But here,\r\nResistance comes from the side and can be straightened out\r\nHands beyond 90 degrees.\r\nIn pulley exercises, the rotating force\r\nThe largest, will always be in the 90-\r\nDegrees between the cable and the arm.', '', '', 0, '', 'Pectoralis Major , Upper and Lower Trapezius , Seratus Anterior', 3, 7, 0, '', 0),
('Crouching', 'abdominals', 'BodyWeight', 'Facilitator: lying on your back, only a bend of the body, not an action from the thigh, since we will perform a hip flexion that strengthens the Iliopsoas', '', '', 0, '', 'Rectus abdominis ', 0, 10, 0, '', 0),
('Curls Dumbbells', 'bicep', 'Weights', 'Arm supported by hip,\r\nThe advantage of this exercise lies in the fact that the trainee can help himself.\r\nInstructions for performing: Sit so that the elbow rests on the thigh and the tip of the elbow touches the end\r\nLower of the knee. When the weight is vertical to the body, the hardest.\r\n', '', '', 1, '', 'Biceps', 6, 9, 0, '', 0),
('Curls Dumbbells Seated', 'bicep', 'Weights', 'This exercise utilizes longitudinal ratios. The slope of the chair changes the voltage length ratios of the Bicep\r\nSince the shoulder is in the raid and is therefore elongated.\r\nIn this situation the Bicep is between D and C (in the length-voltage diagram).\r\nThe movement begins very hard, since all the force of the elbow flexors is for compression.\r\nIn the 100 ° elbow area, this is the optimal position to develop strength.\r\nThe chair should be at a slope of 60 °.\r\nThe elbow flexes and the agonist changes according to the palm position.', '', '', 1, '', 'Biceps', 0, 5, 0, '', 0),
('Curls Preacher Biceps', 'bicep', 'Weights', 'This exercise utilizes the proportional voltage length. The shoulder is bent and therefore\r\nThe Bicep is shortened in the elbow joint (between B and A) and therefore its power development capacity is smaller.\r\nIn this exercise Brachialis works hard.\r\nWhen all the weight of the weight is translated into rotational force, the muscle has the best ability to develop strength.\r\nInstructions for use: The armpit is at the height of the pillow and the chair should be elevated to allow this.\r\nYou can use the weights and perform the hand-hand exercise.\r\n\r\nThe angle of resistance and the angle of force can also be performed against the lower polyps.', '', '', 1, '', 'Biceps ', 0, 6, 0, '', 0),
('Deep Squat', 'Legs', 'Squat', 'goes down about 60 to 45 degrees , a small round of the basin.', '', 'assets/images/Deep Squat.gif', 0, 'A common mistake is to point the knees inside.', 'Gluteus Maximus , Quadriceps , Plantar Flexion , backward erectors.', 4, 8, 0, '', 0),
('downs Press Triceps', 'tricep', 'Machine', 'Elbow Breaks Through Poly Top - downs Press Triceps\r\n(Angled Bar)\r\nAs close as possible to Polly, in order to obtain a range of motion\r\nMax, slightly tilted forward. Step forward slightly bent\r\nKnee forward to create a precise center of gravity. Care must be taken\r\nThat the elbows should be attached to the body in order not to work with\r\nMajor Pectoralis Lower. Have to stand under the beans and not move the\r\nThe shoulder', '', '', 0, '', 'Triceps , Latissimus Dorsi , Teres Major , Lower Pectoralis , Posterior Deltoid', 1, 4, 0, '', 0),
('Dumbbell bench press', 'chest', 'Bench press', 'Weights in the shoulder line, the forearm is always vertical, first movement and end movement. This exercise requires greater stability and greater range of movement at the beginning and end of exercise. (The rotation of the weights at the end of the movement is not necessary because it enters the compaction component.) The largest resistance torque that the arm corresponds to the ground.', '', '', 0, 'A common mistake is to twist your elbows inside.', 'Pectoralis major , Triceps, Seratus Anterior', 6, 10, 0, '', 0),
('elbow bankruptcy', 'tricep', 'Weights', 'elbow bankruptcy with a handbell\r\nOr against the bottom beans on a bench\r\nWhen working against weights, lean with a counter leg near your leg\r\nThe executive. The trunk is parallel to the ground and the elbow is tilted slightly above the line\r\n, The body as much as possible in order to obtain a range of motion\r\nAgainst the underside, the angle of resistance and the angle of force vary\r\nThe rotator. (90 degrees between hand and cable)', '', '', 0, '', 'Triceps , Latissimus Dorsi , Teres Major , Lower Pectoralis , Posterior Deltoid', 4, 7, 0, '', 0),
('Elbow bankruptcy bar', 'tricep', 'Weights', 'Minch: lying down, the shoulder hold of the bar wide, elbows tight, elbow slightly up behind me\r\nForehead and back to straightening hands, this exercise works all Triceps with emphasis on the Head Long\r\nThe largest rotator at 90 degrees, with the mother parallel to the ground (the long head is taut). against\r\nLower poly, variable angle of resistance and angle of force rotator (this exercise is called pressing\r\nFrench)\r\n', '', '', 0, '', 'Triceps , Latissimus Dorsi , Teres Major , Lower Pectoralis , Posterior Deltoid', 7, 10, 0, '', 0),
('elbow flap', 'tricep', 'Weights', 'sitting position: elbow flap behind\r\nThe nape with a hand weight This exercise causes the condition\r\nThe longest of the Triceps. It is important to perform with support\r\nTo the back, a hand supports the elbow from the head and movement\r\nThe length of the range to prevent movement restriction', '', '', 0, '', 'Triceps , Latissimus Dorsi , Teres Major , Lower Pectoralis , Posterior Deltoid', 4, 7, 0, '', 0),
('Flat bench press', 'chest', 'Bench press', 'The person performing the exercise lies on their back on a bench with a weight grasped in both hands. They push the weight upwards until their arms are extended, not allowing the elbows to lock. They then lower the weight to chest level. This is one repetition (rep).', '', '', 0, 'The Bench Press is the most dangerous of all exercises. A dozen of people die each year by dropping the bar on their face, throat or chest during the Bench Press. Tons of other people hurt their shoulders, wrists or back because they Bench Press with bad form.', 'Pectoralis major , Triceps, Seratus Anterior', 4, 8, 0, '', 0),
('Fly Dedicate machine', 'chest', 'Machine', 'The height of the chair should be adjusted\r\nWhich will create an angle of 90 degrees between\r\nThe arm of her mother. Traffic should arrive\r\nFrom the pillow and not just from the elbow, or\r\nWrist. Performing the exercise with\r\nSide iron, will improve the range of motion.\r\nBoth exercises are working on them\r\nMuscles, except for a change in the length of the tension.\r\nWe will keep the Internet to work comfortably\r\nMore in the shoulder because the Altlevor Deltoide\r\nJoins.', '', '', 1, 'A common mistake is unstable elbows.', 'Pectoralis Major , Upper and Lower Trapezius , Seratus Anterior', 2, 6, 0, '', 0),
('Flys', 'chest', 'Weights', 'This exercise is intended primarily to strengthen the Major Pectoralis. Placement: lying down (also possible for a positive slope), exercise\r\nWill be carried out from a finite position with the arms locked / unlocked slightly less than 90 degrees and then removed to the position of the arm\r\nA little below the chest. It is recommended not to lock the elbow for convenience (by locking the elbow, the arms of the load, the torque will be affected\r\nAnd the length of the crane). The more we bend our elbows, the lower the cargo arm and the easier the movement will be. Anyway arm\r\nThe load in the butterfly is greater than the pressure of the chest.', '', '', 0, '', 'Pectoralis Major , Upper and Lower Trapezius , Seratus Anterior', 4, 8, 0, '', 0),
('Front Lift', 'back', 'Weights', 'Lying on the stomach bench, raising\r\nIn the front part by a step for incline\r\nPositive, using a free rod or weights,\r\nThe hands reach almost to the floor in the grip\r\nPosition Mid when performing exercise with\r\nElbows attached to the body. And the mother is always vertical\r\nground. Work in full range of motion. With\r\nA more stable traffic bar can therefore be added\r\nWeight. In the execution of a joint bankruptcy\r\nThe dorsal shoulder will reach maximum contraction\r\nAnd will continue to work in an isometric contraction,\r\nDeltoid Posterior will be an agonist in its place.', '', '', 0, '', 'Latissimus Dorsi , Biceps , Trapezius ', 4, 8, 0, '', 0),
('Front Squat', 'Legs', 'Squat', 'The bar is placed in on the clavicle , with a cross handle. straight back so the resistance moment is easier on the vertebrates.  ', '', '', 1, 'A common mistake is to bend your back.', 'Gluteus Maximus , Quadriceps , Plantar Flexion , backward erectors.\r\n', 3, 7, 0, '', 0),
('Hack Squat', 'Legs', 'Squat', 'Placement the legs will determine the length , the muscular tension of the strippers of the hip.', '', '', 0, 'A common mistake is to not keep the legs close together.', '', 5, 9, 0, '', 0),
('Hand Weights', 'Legs', 'Squat', 'Low center of gravity, an advantage that contributes to stability.', '', '', 0, 'A common mistake is to bend your back.', 'Gluteus Maximus , Quadriceps , Plantar Flexion , backward erectors.', 0, 6, 0, '', 0),
('Hip removal', 'Legs', 'Machine', 'This exercise is mainly intended to strengthen the Medius Gluteus. Standing: Restraining the thigh, a step away from the instrument, exercise\r\nThis is performed with the opposite leg. Cable on the lower part of the ankle and perform a movement of exclusion to 45 degrees.', '', '', 0, '', 'Gluteus medius', 4, 7, 0, '', 0),
('Horizontal Abs', 'abdominals', 'BodyWeight', 'lying on your back, legs bent on the floor, bent to the left: right elbow to the left. Will work internally\r\nLeft. In addition to bending, there is minimal rotation, since there is no resistance that makes rotation difficult,\r\nResistance coming from behind and pushing the elbow down.\r\nThe exercise is not recommended for adults because the vertebrae are squeezed.', '', '', 1, '', 'External , Internal', 0, 10, 0, '', 0),
('Horizontal Rowing', 'shoulders', 'Weights', 'These exercises are designed to strengthen the Deltoid Posterior mainly by lying on the abdomen at a positive slope with hand / rod weights,\r\nFrom the position of a vertical hand to the ground to shoulder height. A dedicated device bar_T', '', '', 1, '', 'Posterior Deltoid , Biceps , Trapezius', 5, 9, 0, '', 0),
('Leg extension', 'Legs', 'Machine', 'Seating, while adjusting the chair, so that the knee will be parallel to the arm axis. Adjusting the ankle pad, if possible,\r\nWill be done so that the pillow will be placed on the ankle joint. You can limit the range of motion on some devices (primarily intended\r\nRehabilitation of injuries). The entire population is recommended to perform work within full muscle movement (full knee surgery).\r\nThe raid will take place throughout the movement and until the knees are straightened. Foot or foot position is not important (closed /\r\nOpen). Backside incline contributes to the extension of femoris Rectus and increased strength. The exercise is intended mainly to strengthen the Quadriceps.', '', '', 0, '', 'Quadriceps', 2, 7, 0, '', 0),
('Leg press', 'Legs', 'Machine', 'High: Hip extensors (Maximus Gluteus and Hamstrings) will work in greater range of motion.\r\nLegs at the top end - Maximus\'s Gluteus strain length curve will increase and thus Maximus Gluteus\'s contribution will increase.\r\nLow: There is a greater load on the knee joint (quadriceps). The lower the angle between the market and the thigh,\r\nThis will increase the pressure on the patella and the level of difficulty will increase. This exercise is similar (45) squat Full / Deep.\r\nAt 45 degrees, the different working muscles are the same. If we position the feet at the highest position and / or increase the\r\nThe angle of the chair, extend the hip extensors.', '', '', 0, 'A common mistake is to put to much weight because it\'s feel easy so you are putting to much pressure on your back.', 'Gluteus Maximus, Quadriceps, Gastrocnemius', 2, 7, 0, '', 0),
('Lifting heels', 'Legs', 'Weights', 'Work in the ankle joint, the difference in the agonist will be according to the knee position. Standing on a step,\r\nLooking forward and lifting heels.', '', '', 1, '', 'Plantar Flexion(Soleus , Gastrocnemius)', 3, 9, 0, '', 0),
('Lunges', 'Legs', 'Weights', 'The exercise is designed to strengthen Maximus Gluteus, the contraction during the descent is eccentric. At the time of immigration\r\nConcentric. Standing: Standing, straight back, big step forward, so we go straight down. A knee comes close to the floor.\r\nStatic mode: A position in which the foot stays in front of the body, without retracting it, and performs only a drop and an increase (dynamic)\r\nFeet forward, perform a descent and an increase and then return the foot to standing position) The descent is carried out with sufficient spreading\r\nLarge to form 90 degrees across all joints (back leg: between hip and market, front leg: between hip and market\r\nAnd between the trunk and the front thigh). The leg that works mostly is the front leg. The back leg is mainly used for stabilizing and\r\nrectus femoris works.', '', '', 1, 'A common mistake is to not going deep enough so you are not working efficiently.  ', 'Gluteus Maximus, Quadriceps, Gastrocnemius', 4, 9, 0, '', 0),
('Lying leg curls', 'Legs', 'Machine', 'The exercise is intended to strengthen the hamstrings. Description: Lying down, adjusting the knee to the arm axis. The joint axis is located\r\nFurther to the road of the machine. The lower part of the machine will be placed above the heel. Due to the ankle position in Dorsi\r\nFlexon, elongated Gastrocnemius and contributes to knee flexion (depending on the voltage length curve)\r\nFlexion Plantar, the gastrocnemius muscles are in shortened state and due to movement, shorten to active failure.\r\nHis contribution to performance will be small as a result - the hamstrings will work harder. This exercise can also be performed in a reclining position,\r\nSitting and standing. The difference between the facilitators will be expressed in the length of stress, the closer we are\r\nTo the anatomical position we can develop more force (lying down).', '', '', 0, 'Rotate pelvis forward to extend voltage length.', 'Hamstring ', 3, 7, 0, '', 0),
('MoveHorizontalShoulder', 'shoulders', 'Machine', 'These exercises are designed to strengthen the Deltoid Posterior. Mainly: Lie on your stomach. The exercise can be performed on an elevated bench\r\nLying down, with your hands in a vertical position to the ground. The exercise should be performed in pronation (which isolates the Deltoid Poster - in the medium position)\r\nWill help.) In order to achieve all the long tension, stay in rotation medially and in pronation for all movement. A slight bending of the elbow decreases\r\nThe load arm and facilitates the exercise. With a cross cable, the movement can be extended, because the direction of resistance is different and the angle of resistance.\r\nPeak contraction at the end of movement. The swing is in a straight hand with a slight bent and it reaches a position of a uniform line between the elbow, shoulder and dumbbell. Complex exercise\r\nWill be horizontal rowing.', '', '', 0, '', 'Posterior Deltoid , Triceps ', 5, 9, 0, '', 0),
('narrow grip bench', 'tricep', 'Weights', 'These exercises are designed to strengthen the Triceps, a narrow grip. Elbows down to the body.\r\n\r\nCan be performed with a free rod or with an SM, the rod will come under the chest, her mother vertically.', '', '', 0, '', 'Triceps , Upper & Lower Trapezius , Seratus Anterior , Anterior Deltoid', 8, 10, 0, '', 0),
('Negative bench press', 'chest', 'Bench press', 'These exercises are designed to strengthen mainly the Major Pectoralis Lower.\r\nPosition: lying down, bench at a negative angle of 30 degrees, the weight will be greater\r\nThan in the other types of exercises. This is due to Synergist Dorsi Latissimus.\r\nThe download will go down to the lowest point in the sternum. Not convenient to perform.', '', '', 0, '', 'Lower pectoralis major , Triceps , Upper & Lower Trapezius , Seratus Anterior', 4, 8, 0, '', 0),
('Parallel Bar Dips', 'chest', 'Machine', 'This exercise is intended primarily to strengthen the Major Pectoralis Lower. If the elbows are tight: we will work mainly on the Triceps\r\nThe elbow, the shoulder joint, the Deltoid Anterior- and the Pectoralis will work. Hip bending is performed to ease the performance, tilt the body\r\nforward. Perform the exercise with the back tilted forward and the elbows out.', '', '', 1, 'A common mistake is to point with elbows out.', 'Lower Pectoralis Major , Triceps , Hyper Extensions', 3, 7, 0, '', 0),
('Parallel in short grip', 'tricep', 'Machine', 'These exercises are designed to strengthen the Triceps, a narrow grip. Elbows down to the body.\r\nThis wide grip when the body is tilted forward, buttocks back and elbows out.', '', '', 0, '', 'Triceps , Anterior Deltoid ', 4, 10, 0, '', 0),
('Poly Closed', 'back', 'Machine', 'Minch: sitting, sitting with a slight back tilt, emphasis\r\nOn elbows close to the body, lowered down under the chin.\r\nMaking the finish would be easier, since Dorsi Latissimus\r\nIs in its extended state - and Biceps works more. (More close\r\nC in the voltage length table and therefore have more range of motion\r\nBiceps main) immediately pinching narrow grip (auxiliary in both\r\nLoops) have a longer brachialis power output\r\nWhat Biceps - 3 - and Muscles Work With All Strength.\r\nPull with upper poly (wide grip) to the chest\r\nMenach: sitting, straight back with a slight tilt back, elbows\r\nBy lowering to the sides of the body, the palm is in the hand of the nose\r\nBrachioraDialis is a bit more dominant but the Biceps are gentle\r\nmain. Good angle to perform Heather Age: elbow at 90 degree\r\nThe download is until the bar touches the top of the tray\r\nChest, keep a pelvis forward so that the chest protrude, wrong\r\nTake your back because there is a transition\r\nWorking on the Deltoid Posterior and not with the Latissimus\r\n(Dorsi\r\n(There is no difference in operation when the poly is passed behind\r\nNape or front)', '', '', 1, 'Pulling your arms forward. Which causes the chest to work a bit more. Those who prevent this erroneous movement are shoulder muscles\r\nWhich rotate vertically. (In a broad grip, the antagonist will be a shoulder distance Deltoid Middle.)', 'Latissimus Dorsi , Biceps , Brachialis , Trapezius ', 1, 6, 0, '', 0),
('Poly Extension', 'tricep', 'Machine', 'Pulling the elbows through the upper polyps in tilting the back with the back to Polly does not work in the shoulder only\r\nAt the elbow, hands attached to the body.\r\nstabilizers :\r\nSpinal flexors (abdominal muscles)\r\n(Iliopsoas, Rectus Femoris)\r\n(Latissimus Dorsi, Teres Major, Lower Pectoralis, Posterior Deltoid) Shoulder Busters\r\n\r\nYou can also perform lower polyps with the back to the beans, a slight tilt, a slight forward step, and a raid', '', '', 0, '', 'Triceps , Latissimus Dorsi , Teres Major , Lower Pectoralis , Posterior Deltoid', 0, 4, 0, '', 0),
('Poly Open', 'back', 'Machine', 'Pull with upper poly (narrow grip) to the chin\r\n(Finination and pronation in the usual rod) for immediately there\r\nReplace rod))\r\nMinch: sitting, sitting with a slight back tilt, emphasis\r\nOn elbows close to the body, lowered down under the chin.\r\nMaking the finish would be easier, since Dorsi Latissimus\r\nIs in its extended state - and Biceps works more. (More close\r\nC in the voltage length table and therefore have more range of motion\r\nBiceps main) immediately pinching narrow grip (auxiliary in both\r\nLoops) have a longer brachialis power output\r\nWhat Biceps - 3 - and Muscles Work With All Strength.\r\nPull with upper poly (wide grip) to the chest\r\nMenach: sitting, straight back with a slight tilt back, elbows\r\nBy lowering to the sides of the body, the palm is in the hand of the nose\r\nBrachioraDialis is a bit more dominant but the Biceps are gentle\r\nmain. Good angle to perform Heather Age: elbow at 90 degree\r\nThe download is until the bar touches the top of the tray\r\nChest, keep a pelvis forward so that the chest protrude, wrong\r\nTake your back because there is a transition\r\nWorking on the Deltoid Posterior and not with the Latissimus\r\n(Dorsi', '', '', 1, 'Pulling your arms forward. Which causes the chest to work a bit more. Those who prevent this erroneous movement are shoulder muscles\r\nWhich rotate vertically. (In a broad grip, the antagonist will be a shoulder distance Deltoid Middle.)', 'Latissimus Dorsi , Biceps , Brachialis , Trapezius ', 1, 6, 0, '', 0),
('Poly Pull', 'back', 'Weights', 'Rowing with bottom polly,\r\nStanding, tilting forward, one hand\r\nHolding Polly for stability, and a hand\r\nHolding on to Polly that the force from below,\r\nPerform a shoulder raid and elbow\r\nBend, forefoot and bent back\r\nStraight. For stability. Exercise\r\nJust like the picture above, only\r\nStanding and with lower polly.', '', '', 0, '', 'Latissimus Dorsi , Biceps , Trapezius ', 0, 5, 0, '', 0),
('Positive bench press', 'chest', 'Weights', 'These exercises are designed to strengthen mainly the Major Pectoralis Upper.\r\nPlacement: lying down, will be performed with a bench at a favorable angle of 45-40 degrees.\r\nWith a free rod, the stabilizers will work. With free weights, the stabilizers will work\r\nAt the highest level therefore, the skill should be the highest.\r\nThe rod will be lowered just below the collarbone.\r\nIt can be done in the following forms:\r\nFree Bench Press with Push Bench Flat, Handweight, SM', '', '', 1, '', 'Upper pectoralis major , Triceps , Upper & Lower Trapezius , Seratus Anterior', 4, 8, 0, '', 0),
('Positive Fly', 'chest', 'Weights', 'Exercises are designed to strengthen the main Pectoralis Upper. Menach: lying at a positive slope 45-40 degrees. With\r\nHand weights End the movement with the rest of the hands standing to the ground. The cable has a range of motion beyond normal.', '', '', 0, '', 'Upper pactoralis Major , BBB , Upper & Lower Trapezius , Seratus Anterior', 3, 7, 0, '', 0),
('Pull Over', 'back', 'Machine', 'Minch: Stand up, shoulder raid against upper polyps, take a step and tilt forward (greater range of motion).\r\nMin: Lying with a hand / pole weight: Exercise is performed until the hands are straight up above the shoulder, otherwise the movement\r\nTo be carried out would be a sit-down and not a raid. The elbow elbows work only when they are flexing and arm-wrestling\r\nThe shoulder. Flexing the elbow reduces the load arm and the torque is smaller. Perform exercise up to 180 and not beyond.', '', '', 0, '', 'Latissimus Dorsi , Trapezius', 1, 6, 0, '', 0),
('Pull-ups', 'back', 'Machine', 'These exercises are mainly designed to strengthen the Dorsi Latissimus.\r\nIn all of the grips, the same muscles will only work in Action Reverse. That is to say,\r\nOrigin The fixation of the distal part of a root so that the hand, combined with the movement of the arm towards the forearm, forms a chain of motion\r\nClosed.', '', '', 1, '', 'Latissimus Dorsi , Biceps , Trapezius', 4, 9, 0, '', 0),
('Saws', 'back', 'Weights', 'Using a bench, standing on six\r\nWith hand weights, the hand works\r\nAnd the leg that works on the same side. The knee\r\nThe inactive is placed on the bench.\r\nThe supporting hand is locked. The body must be\r\nStraight. A slight rotation of the pelvis forward.\r\nThe movement is carried out by the shoulder,\r\nWhen the arm remains vertical to the ground,\r\n, Straight and vertical arm under the body,\r\nElbow attached to the body. Raid operations\r\nIn the shoulder and flexion at the elbow, movement\r\nIs carried out just beyond the body line\r\n(If the elbow passes the trunk line in the form\r\nOh, the Deltoid Posterior\r\nWill be dominant.) Take weight training\r\nHigh avoidance errors in execution).\r\nCommon mistakes: rounded back, elbow\r\nNot attached to the body, wrong base - hand\r\nNot attached to the knee but below\r\nShoulder. And the movement is led from the shoulder\r\nAnd not from the elbow.', '', '', 0, '', 'Latissimus Dorsi , Biceps , Trapezius ', 6, 9, 0, '', 0),
('Seated Bend', 'shoulders', 'Weights', 'This exercise is designed to strengthen the Deltoid Anterior mainly: lying at a 60 degree angle with hand weights. This is an exercise\r\nIsolated / simple. Making with a rod will be performed in pronation.\r\nPerforming the exercise: lying down or sitting on a bench with a positive slope. The shoulder bends start the movement from an elongated position rather than from a line\r\nThe body. This is the application of the classical voltage length curve. In any case, we will not stand up because the exercise will take place outside the center\r\nThe body will have a large load on the back (also, in a sitting position without a gradient).', '', '', 0, 'Must do the exercise seated with a bit inclined', 'Anterior Deltoid , Upper & Lower Trapezius , Seratus Anterior', 7, 10, 0, '', 0),
('Seated Pulley Row', 'back', 'Machine', 'The trunk is straight back but not moving,\r\nKnees bent to neutralize the tension\r\nMuscle and reduce strain on the vertebrae.\r\n(When the leg is straight, the pelvis will be rotated backwards\r\nAnd we want to keep the pelvis turning forward\r\nSo as not to shorten the hamstring) elbows\r\nClose to the body (in a narrow grip with a second rod\r\nHandles no difference) movement towards the part\r\nThe lower abdomen. 100% Rotating component -\r\nThroughout the movement. With respect to weight\r\nPulls the body into the device (the body\'s doubles\r\nStabilizers) An angle is created between the cable and the organ\r\nOf 90 degrees (whose muscles are paired\r\nHis trunk is not strong enough not recommended.\r\nMore for the sports required)\r\nBecause the breast is not supported and created\r\nClick the lumbar vertebrae', '', '', 0, '', 'Latissimus Dorsi , Biceps , Trapezius ', 2, 6, 0, '', 0),
('shoulder blades', 'shoulders', 'Weights', 'These exercises are designed to strengthen the Trapezius Upper. Resistance comes from below.', '', '', 0, '', 'Trapezius Upper ** (another synergist Scapula Lavator', 2, 6, 0, '', 0),
('Shoulder Press', 'shoulders', 'Weights', 'Broad grip: ninety degrees between the pole and near. Elbows on her sides, her mother vertical. Enable both forward and backward.\r\nIn a narrow grip: the Deltoid Anterior (and a slight incline), the agonist will be Deltoid Anterior and the synergists:\r\nSpeak it and the elbows at the elbow, Upper Pectoralis, Coracobrachialis, Middle Deltoid\r\nWhen working with weights against a rod, the range of motion is greater and there are more stabilizers (chest and torsion)\r\nWhen working with Machine Smith, the operation is the same as the stabilizers.\r\n\r\nYou can do the following:\r\n(Back) or front, hand weights, special machine, SM (front or back) in SM\r\n', '', '', 0, '', 'Middle Deltoid , Triceps , Trapezius , Seratus Anterior', 7, 10, 0, '', 0),
('Shoulder removal', 'shoulders', 'Machine', 'In the frontal removal exercise, we will activate all three Deltoid heads (Posterior works as a stabilizer).\r\nIn the first thirty degrees, the main force will come from the Supraspinatus, because the Deltoid has a small application angle in the thirty degrees\r\nAnd it is difficult for him to produce alone the strength required to shrink.\r\nThe distance range is up to 120 degrees (above the shoulder height), Deltoid reaches the peak of the contraction at an angle of 120 degrees, mode A, the shortest by\r\nLength curve voltage. Transition (up to 180 degrees) Enter as synergists, the Interior Seratus and Trapezius Lower & Upper will work as a pair\r\nForces (in the withers) due to the removal of the amount of rotational rotation.\r\n', '', '', 0, 'The exercise can be performed in three ways:\r\nExternal - Anterior Assistive, Main Middle Posterior becomes an antagonist.\r\nFull range of motion, the head of the yomros is not stuck at the top of the ecumen, the movement is shifted backward.\r\nInternal rotation - With the removal of a shoulder with a machine. The range of motion is relatively limited due to acromion.\r\nPosterior\'s involvement as a larger synergist Middle. Descending\r\nPosterior, Main Anterior) - Neutral (Mid Position', 'Deltoid Middle entered Deltoid Posterior as a synergist\r\nTriceps - If the elbow is bent slightly\r\nAnterior Seratus (+ lateral rotation) Trapezius', 4, 8, 0, '', 0),
('Side bending', 'abdominals', 'BodyWeight', 'You can perform standing, bottom beans, Roman bench, lying on a mattress\r\nEccentric and concentric contraction is performed simultaneously.', '', '', 1, '', 'Transversus abdominis ', 0, 10, 0, '', 0),
('Smith machine', 'Legs', 'Squat', 'Stability is achieved through\r\nSmash, lesser work of\r\nThe erectors of the back. In this situation\r\nWe extended the hip extensors and they\r\nLarge-voltage workers\r\nMore because we start\r\nThe traffic is bent with\r\nLeaning on the bar, legs\r\nA little forward and therefore there is more\r\nLength of voltage.', '', '', 0, 'A common mistake is a drain inward', 'Gluteus Maximus , Quadriceps , Plantar Flexion , backward erectors.', 2, 7, 0, '', 0),
('Static ', 'abdominals', 'BodyWeight', 'Can be done in two mode or on the forearms', '', '', 1, '', 'Transversus abdominis ', 0, 10, 0, '', 0),
('Stiff Leg Dead Lift', 'Legs', 'Weights', 'Working with the hip joint, rotating the pelvis to the face, straight gouge, recommended integrated grip in the rod, pronation and finination. Starting with\r\nThe foot is straight and then the knees are slightly gloved, rolling the rod close to the body from the pelvis until slightly after the knees. Hamstrings stretched, working\r\nMore on the buckles. In this exercise there is a passive loss of Hamstrings when performing the exercise with straight legs.\r\nWhen we perform the whole exercise in the right legs, the Hamstrings will be in a state of passive insufficiency and therefore the Gluteus\r\nMaximus will work harder.', '', '', 0, 'A common mistake is to bend your back', 'Hamstrings (Maximus Gluteus) Synergist (the hamstring is elongated and develops more force)\r\nSpine (isometric-stabilizing stabilizers)\r\nCan be performed with free rod and hand weights.', 6, 10, 0, '', 0),
('Straight rowing', 'shoulders', 'Weights', 'The grip on the rod should be narrower than the shoulder width, toe to toe. The rod is attached to the body throughout the movement and reaches the chin.\r\nElbows turned upwards.\r\nWhen using lower bolts, you should not stand too far because:\r\nA. We strive for the rowing movement and the farther we move, the closer the movement will be to the horizontal raid / exclusion.\r\nB. We want to avoid the spin component on the spine.', '', '', 0, '', 'Middle Deltoid , Biceps , Trapezius , Seratus Anterior', 3, 6, 0, '', 0),
('T_Bar', 'back', 'Weights', 'In a special machine, the height of the seat should be\r\nSo the support cushion will be at chest level.\r\nPillow distance: In a way that will cause a constant load.\r\nChest support reduces the pressure on the\r\nThe interstitial disks cancel the\r\nZuckfei\'s work as stabilizers\r\nIn a BAR_T-machine do not hold that wide then\r\nWork on the Delaware and Delaware Post\r\nlatissimus dorsi.', '', '', 0, '', 'Latissimus Dorsi , Biceps , Trapezius ', 4, 8, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `prog_id` int(11) UNSIGNED NOT NULL,
  `prog_name` varchar(32) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `prog_grade` float UNSIGNED NOT NULL,
  `trainee_grade` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `programs_exercises`
--

CREATE TABLE `programs_exercises` (
  `program_id` int(11) UNSIGNED NOT NULL,
  `exerciseName` varchar(22) NOT NULL,
  `reps` tinyint(4) DEFAULT NULL,
  `sets` tinyint(4) DEFAULT NULL,
  `weight` tinyint(4) DEFAULT NULL,
  `rest` time DEFAULT NULL,
  `categoryGroup` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `prog_id` int(11) UNSIGNED NOT NULL,
  `exerciseName` varchar(22) NOT NULL,
  `reps` tinyint(4) DEFAULT NULL,
  `sets` tinyint(4) DEFAULT NULL,
  `rest` datetime DEFAULT NULL,
  `weight` tinyint(4) DEFAULT NULL,
  `sessionDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trainees`
--

CREATE TABLE `trainees` (
  `id` int(11) UNSIGNED NOT NULL,
  `userName` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `status` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `target` tinyint(1) UNSIGNED DEFAULT NULL,
  `age` smallint(3) UNSIGNED DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `weight` smallint(3) DEFAULT NULL,
  `height` float DEFAULT NULL,
  `body_fat` tinyint(3) DEFAULT NULL,
  `health_condition` text,
  `intensity` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trainees`
--

INSERT INTO `trainees` (`id`, `userName`, `email`, `status`, `first_name`, `last_name`, `target`, `age`, `gender`, `weight`, `height`, `body_fat`, `health_condition`, `intensity`) VALUES
(14, 'asaf', 'asaf1234@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `userName` varchar(32) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(32) NOT NULL,
  `isAdmin` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userName`, `password`, `email`, `isAdmin`) VALUES
(46, 'asaf', '$2y$10$GLnGeKZ.t5j/1Ed5HDiUP.HhT/8wtSTOMj2uqk1XzlzU.kJWhYF1.', 'asaf1234@gmail.com', 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_debug`
--
ALTER TABLE `admin_debug`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userName` (`userName`);

--
-- Indexes for table `exercises`
--
ALTER TABLE `exercises`
  ADD PRIMARY KEY (`exerciseName`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`prog_id`),
  ADD UNIQUE KEY `user_name` (`user_name`) USING BTREE;

--
-- Indexes for table `programs_exercises`
--
ALTER TABLE `programs_exercises`
  ADD KEY `program_id` (`program_id`) USING BTREE,
  ADD KEY `exerciseName` (`exerciseName`) USING BTREE;

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD KEY `userName` (`prog_id`) USING BTREE,
  ADD KEY `exerciseName` (`exerciseName`) USING BTREE;

--
-- Indexes for table `trainees`
--
ALTER TABLE `trainees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`userName`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userName` (`userName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_debug`
--
ALTER TABLE `admin_debug`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `prog_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `trainees`
--
ALTER TABLE `trainees`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_debug`
--
ALTER TABLE `admin_debug`
  ADD CONSTRAINT `admin_debug_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `users` (`userName`);

--
-- Constraints for table `programs`
--
ALTER TABLE `programs`
  ADD CONSTRAINT `programs_ibfk_1` FOREIGN KEY (`user_name`) REFERENCES `trainees` (`userName`);

--
-- Constraints for table `programs_exercises`
--
ALTER TABLE `programs_exercises`
  ADD CONSTRAINT `programs_exercises_ibfk_1` FOREIGN KEY (`program_id`) REFERENCES `programs` (`prog_id`),
  ADD CONSTRAINT `programs_exercises_ibfk_2` FOREIGN KEY (`exerciseName`) REFERENCES `exercises` (`exerciseName`);

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_1` FOREIGN KEY (`prog_id`) REFERENCES `programs` (`prog_id`),
  ADD CONSTRAINT `session_ibfk_2` FOREIGN KEY (`exerciseName`) REFERENCES `exercises` (`exerciseName`);

--
-- Constraints for table `trainees`
--
ALTER TABLE `trainees`
  ADD CONSTRAINT `trainees_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `users` (`userName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
