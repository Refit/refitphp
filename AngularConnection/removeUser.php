<?php

require "../Core/dbClass.php";

header('Access-Control-Allow-Origin: http://localhost:4200');
header('Content-type: application/json');
//header("Content-Type: text/plain");

# Get JSON as a string
$json_str = file_get_contents('php://input');
//  var_dump($json_str);
# Get as an object
$json_obj = json_decode($json_str);

$db = new dbClass();

$res = $db->deleteUserByName($json_obj);

echo json_encode($res);

//echo json_encode($json_obj->exerciseName);


?>