<?php

require "../Core/Exercise/Exercise.php";
require "../Core/dbClass.php";

header('Access-Control-Allow-Origin: http://localhost:4200');
header('Content-type: application/json');
//header("Content-Type: text/plain");

# Get JSON as a string
$json_str = file_get_contents('php://input');
//  var_dump($json_str);
# Get as an object
$json_obj = json_decode($json_str);


//var_dump($json_obj);
$test = new dbClass();

$ex = new Exercise();


$ex->exerciseName = $json_obj->exerciseName;

$ex->catagory = $json_obj->category;

$ex->exerciseType = $json_obj->exerciseType;
$ex->instructions = $json_obj->instructions;

$ex->video = $json_obj->video;

$ex->picture = $json_obj->picture;

$ex->recommanded = $json_obj->recommended;   

$ex->mistakes = $json_obj->mistakes;

$ex->activityGroup = $json_obj->activityGroup;
$ex->complexMin = $json_obj->complexMin;
$ex->complexMax = $json_obj->complexMax;
$ex->health = $json_obj->health;
$ex->motivation = $json_obj->motivation;
$ex->isFast = $json_obj->isFast;

$res = $test->insertExercise($ex);


echo json_encode($res);

//echo json_encode($json_obj->exerciseName);


?>